package models

import (
	"github.com/jinzhu/gorm"
)

// UserStruct struct type
type UserStruct struct {
	gorm.Model
	Username    string `sql:"type:VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci"`
	Password    string `sql:"type:VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci"`
	Email       string `sql:"type:VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci"`
	Description string `sql:"type:TEXT CHARACTER SET utf8 COLLATE utf8_general_ci"`
	PathImage   string `sql:"type:VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci"`
}

// UserTokenStruct struct type
type UserTokenStruct struct {
	gorm.Model
	Token           string `sql:"type:VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci"`
	UserStructRefer uint
}

// ObjStruct struct type
type ObjStruct struct {
	gorm.Model
	Name        string `sql:"type:VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci"`
	Description string `sql:"type:TEXT CHARACTER SET utf8 COLLATE utf8_general_ci"`
	PathImage   string `sql:"type:VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci"`
}

// TestStruct struct type
type TestStruct struct {
	gorm.Model
	Name string `sql:"type:VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci"`
}
