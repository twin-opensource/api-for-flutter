package helpers

import (
	"errors"
	"fmt"
	"log"
	"strings"
	"time"

	"github.com/revel/revel"

	jwt "github.com/dgrijalva/jwt-go"
	"golang.org/x/crypto/bcrypt"
)

var (
	errAuthHeaderNotFound = errors.New("authorization header not found")
	errInvalidTokenFormat = errors.New("token format is invalid")
)
var hmacSecret = []byte{61, 56, 115, 83, 90, 56, 116, 125, 78, 110, 38, 113, 122, 93, 90, 54, 54, 84, 96, 104, 62, 62, 76, 59, 35, 37, 85, 72, 74, 50, 70, 83}

// HashPassword func
func HashPassword(password string) (string, error) {
	bytes, err := bcrypt.GenerateFromPassword([]byte(password), 14)
	return string(bytes), err
}

// CheckPasswordHash func
func CheckPasswordHash(password, hash string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(password))
	return err == nil
}

//EncodeTokenMobile func
func EncodeTokenMobile(encode string) string {
	tn := time.Now()
	loc, _ := time.LoadLocation("Africa/Abidjan")
	t := tn.In(loc)
	iss := revel.Config.StringDefault("twinsynergy.token.iss", "")
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"code": encode,
		"nbf":  time.Date(t.Year(), t.Month(), t.Day(), t.Hour(), t.Minute(), t.Second(), t.Nanosecond(), time.UTC).Unix(),
		"iss":  iss,
	})
	tokenString, err := token.SignedString(hmacSecret)
	fmt.Println(tokenString, err)
	return tokenString
}

//DecodeToken func
func DecodeToken(tokenString string) (jwt.MapClaims, error) {
	fmt.Println("tools.DecodeToken")
	token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
		}
		return hmacSecret, nil
	})
	claims, ok := token.Claims.(jwt.MapClaims)
	if ok && token.Valid {
		fmt.Println("claims ok:", claims)
	} else {
		log.Println(err)
		return nil, err
	}
	return claims, nil
}

//GetTokenString func
func GetTokenString(authHeader string) (tokenString string, err error) {
	if authHeader == "" {
		log.Println(errAuthHeaderNotFound)
		return "", errAuthHeaderNotFound
	}

	tokenSlice := strings.Split(authHeader, " ")
	if len(tokenSlice) != 2 {
		return "", errInvalidTokenFormat
	}
	tokenString = tokenSlice[1]
	return tokenString, nil

}

// CheckErr func
func CheckErr(err error, msg string) {
	if err != nil {
		log.Println(msg)
	}
}
