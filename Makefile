local-up:
	docker-compose -f local.yml build
	docker-compose -f local.yml up -d
local-start:
	docker-compose -f local.yml start
local-stop:
	docker-compose -f local.yml stop
local-log:
	docker-compose -f local.yml logs -f app
local-glide:
	docker-compose -f local.yml exec app glide install
local-restart:
	docker-compose -f local.yml restart app
	
dev-up:
	docker-compose -f dev.yml build
	docker-compose -f dev.yml up -d
dev-start:
	docker-compose -f dev.yml start
dev-stop:
	docker-compose -f dev.yml stop
dev-restart:
	docker-compose -f dev.yml restart app
dev-log:
	docker-compose -f dev.yml logs -f app
dev-check:
	docker-compose -f dev.yml ps
dev-glide:
	docker-compose -f dev.yml exec app glide install